#!/usr/bin/env python3
import json, os
from message import *

class MessageCatalog:
	#constructor, loads the messages (if there are any)
	#in this implementation we store all messages in a json file
	def __init__(self, filepath):
		#            I_SAID,I_HEARD,THEY
		self.data = [  []  ,  []   , [] ]# on crée une liste vide
		self.filepath = filepath# on initialise filepath avec la valeur que l'on a voulu
		if os.path.exists(self.filepath):#si le filepath existe, on l'ouvre sinon on l'enregistre
			self.open()
		else:
			self.save()

	#destructor closes file.
	def __del__(self):
		del self.filepath# on ferme le filepath
		del self.data# on supprime la data

	#get catalog size
	def get_size(self, msg_type=False):
		if(msg_type is False):#si on ne connait pas les types des messages
			res = self.get_size(Message.MSG_ISAID) + self.get_size(Message.MSG_IHEARD) + self.get_size(Message.MSG_THEY)#on additionne les tailles de chaque type de messages
		else:
			res = len(self.data[msg_type])#on récupère la taille du catalogue pour un type de message
		return res

	#Import object content
	def c_import(self, content, save=True):
		i = 0# on initialise un compteur
		for msg in content:
			#we don't save after adding message because we just
			#read the content
				if self.add_message(Message(msg), False):# on ajoute un a un tous les messages a la liste
					i=i+1# on met a jour le compteur
		if save:
			self.save()#on sauvegarde la liste sur un fichier
		return i

	#Import a set of messages but changing their type to new_type
	def c_import_as_type(self, content, new_type=Message.MSG_THEY):
		i=0# on initialise un compteur
		for msg in content:
			m = Message(msg)# on crée un message
			m.set_type(new_type)#on initialise le type du message
			if self.add_message(m, False):#si le message n'est pas deja sauvegarder, on ajoute le message au catalogue
				i=i+1# on met a jour le compteur
		if i > 0:
			self.save()
		return i

	#Open = load from file
	def open(self):
		file = open(self.filepath,"r")#on ouvre un fichier
		self.c_import(json.load(file), False)#on charge le contenu du fichier et on l'importe
		file.close()#on ferme le fichier

	#export the messages of a given type from a catalog
	def c_export_type(self, msg_type, indent=2, bracket=True):
		tmp = int(bracket) * "[\n"
		first_item = True
		for msg in self.data[msg_type]:
			if first_item:
				first_item=False
			else:
				tmp = tmp +",\n"
			tmp = tmp + (indent*" ") +  msg.m_export()
		tmp = tmp + int(bracket) * "\n]"
		return tmp

	#Export the whole catalog under a text format
	def c_export(self, indent=2):
		tmp = ""
		if(len(self.data[Message.MSG_ISAID])> 0 ):# on vérifie si on a un message ISAID
			tmp = tmp + self.c_export_type(Message.MSG_ISAID, indent, False)# on exporte le message ISAID
		if(len(self.data[Message.MSG_IHEARD]) > 0):# on vérifie si on a un message IHEARD
			if tmp != "":
				tmp = tmp + ",\n"# on autorise le retour a la ligne si tmp n'est pas vide
			tmp = tmp + self.c_export_type(Message.MSG_IHEARD, indent, False)# on exporte le message IHEARD
		if(len(self.data[Message.MSG_THEY]) > 0):# on vérifie si on a un message THEY
			if tmp != "":
				tmp = tmp + ",\n" # on autorise le retour a la ligne si tmp n'est pas vide
			tmp = tmp + self.c_export_type(Message.MSG_THEY, indent, False)# on exporte le message THEY
		tmp = "[" + tmp + "\n]"# on encadre tmp dans [...\n]
		return tmp

	#a method to convert the object to string data
	def __str__(self):
		tmp="<catalog>\n"
		tmp=tmp+"\t<isaid>\n"
		for msg in self.data[Message.MSG_ISAID]:
			tmp = tmp+str(msg)+"\n"
		tmp=tmp+"\n\t</isaid>\n\t<iheard>\n"
		for msg in self.data[Message.MSG_IHEARD]:
			tmp = tmp+str(msg)+"\n"
		tmp=tmp+"\n\t</iheard>\n\t<theysaid>\n"
		for msg in self.data[Message.MSG_THEY]:
			tmp = tmp+str(msg)+"\n"
		tmp=tmp+"\n\t</theysaid>\n</catalog>"
		return tmp

	#Save object content to file
	def save(self):
		file = open(self.filepath,"w")# on ouvre un fichier
		file.write(self.c_export())# on écrit dans le fichier le contenu du catalogue
		file.close()# on ferme le fichier
		return True

	#add a Message object to the catalog
	def add_message(self, m, save=True):
		res = True
		if(self.check_msg(m.content, m.type)):# si le message est bien présent
			print(f"{m.content} is already there")# on écrit le message 
			res = False
		else:#si il n'est pas present
			self.data[m.type].append(m)# on l'ajoute a la liste
			if save:
				res = self.save()# on sauvegarde le message
		return res# on retourne res qui précise si un ajout a eu lieu ou pas

	#remove all messages of msg_type that are older than max_age days
	def purge(self, max_age=14, msg_type=False):
		if(msg_type is False):
			self.purge(max_age, Message.MSG_ISAID)#on enlève les message ISAID avec un age supérieur au max_age souhaité
			self.purge(max_age, Message.MSG_IHEARD)#on enlève les message IHEARD avec un age supérieur au max_age souhaité
			self.purge(max_age, Message.MSG_THEY)#on enlève les message THEY avec un age supérieur au max_age souhaité
		else:
			removable = []# on crée une liste avec les messages a supprimer. initialement elle est vide
			for i in range(len(self.data[msg_type])):
				if(self.data[msg_type][i].age(True)>max_age):#si l'age du message est supérieur à l'age max 
					removable.append(i)# on ajoute le message a la liste des messages supprimés
			while len(removable) > 0:
					del self.data[msg_type][removable.pop()]# on supprime les messages a la fois du catalogue de message et a la fois du catalogue "removable". on le fait jusqu'a ce que la liste "removable" soit vide
			self.save()#on met a jour le catalogue
		return True

	#Check if message string is in a category
	def check_msg(self, msg_str, msg_type=Message.MSG_THEY):
		for msg in self.data[msg_type]:#on cherche les message d'un certain type (MSG_THEY)
			if msg_str == msg.content:# si le message est déjà dans la liste
				return True# on retourne True si on trouve le message
		return False#on retourne false si on ne trouve pas le message

	#Say if I should quarantine based on the state of my catalog
	def quarantine(self, max_heard=4):
		self.purge()# on supprime les message grâce a la fonction purge au desssu
		n = 0# on initialise un compteur a 0
		for msg in self.data[Message.MSG_IHEARD]:#on cherche les message d'un certain type (MSG_IHEARD)
			if self.check_msg(msg.content):#si jamais la classe check_msg est validée, c'est a dire qu'on a trouvé le message souhaité dans le catalogue
				n = n + 1# on augmente le compteur de 1
		print(f"{n} covid messages heard")# on affiche le nombre de message "heard"
		return max_heard < n# si jamais notre nombre n de message entendu est supérieur à la limite que l'on a fixé a 4

#will only execute if this file is run
if __name__ == "__main__":
	#test the class
	catalog = MessageCatalog("test.json")# On crée un catalogue
	catalog.add_message(Message())#on ajoute un message sans attributs
	catalog.add_message(Message(Message.generate(), Message.MSG_IHEARD))#on ajoute un message avec des attributs
	print(catalog.c_export())#on affiche tous les messages du catalogue en fonction du type
	time.sleep(0.5)#on met un délai de 0,5 avant de réaliser la ligne suivant
	catalog.add_message(Message(f"{{\"content\":\"{Message.generate()}\",\"type\":{Message.MSG_THEY}, \"date\":{time.time()}}}"))# on ajoute un message avec un type et un temps
	catalog.add_message(Message())#on ajoute un message sans attributs
	print(catalog.c_export())#on affiche tous les messages du catalogue en fonction du type
	time.sleep(0.5)#on met un délai de 0,5 avant de réaliser la ligne suivant
	catalog.add_message(Message())
	print(catalog.c_export())#on affiche tous les messages du catalogue en fonction du type
	time.sleep(2)#on met un délai de 2 avant de réaliser la ligne suivant
	catalog.purge(2)#on supprime tous les messages plus vieux qu'un temps de 2 unités de temps
	print(catalog)

