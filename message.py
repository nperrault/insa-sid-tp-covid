#!/usr/bin/env python3
import json
#import time
#replaced by the following two lines for testing purposes
import falsifytime
time = falsifytime.falsifytime()

#for generation of messages
from random import random
from hashlib import sha256

class Message:
	#some constants (message types)
	MSG_ISAID = 0 	#What-I-said message
	MSG_IHEARD = 1	#What-I-heard message
	MSG_THEY = 2	#What-covid-19-infected-said message
	#the constructor can take various parameters
	#default is parameterless, it generates a message to be said
	#if type is not specified, it is to be imported from an export string or parsed export string
	#otherwise, if date is False the time is system time
		#else it is a float, then the date should be this parameter
	def __init__(self, msg="", msg_type=False, msg_date=False):
		if msg == "":#si mon message est vide
			self.content = Message.generate()	#on initialise l'attribut content en utilisant la fonction generate dessous qui génere un message aléatoirement
			self.type = Message.MSG_ISAID#l'attribut type a la valeur de MSG_ISAID donnée au dessus
			self.date = time.time()# on récupère le temps au moment de créer le message
		elif msg_type is False :	#si on ne connait pas le type du message
			self.m_import(msg)	#on fait appel à la fonction m_import dessous
		else:#si on  connait le type du message et il a un contenu
			self.content = msg	#on récupère le contenu du message
			self.type = msg_type#on récupère le type du message
			if msg_date is False:	#si il n'a pas de date associée, on lui donne la date actuelle
				self.date = time.time()
			else:# si il a une date, on la récupère
				self.date = msg_date

	#a method to print the date in a human readable form
	def hdate(self):
		return time.ctime(self.date)# on récupère la date au format lisible par un homme

	#a method to compute the age of a message in days or in seconds
	#for display purpose, set as_string to True
	def age(self, days=True, as_string=False):
		age = time.time() - self.date # on calcule l'age en faisant la soustraction de la date actuelle, moins la date de création
		if days:
			age = int(age/(3600*24))# on convertit l'age en journée
		if as_string:
			d = int(age/(3600*24))#on obtient le nombre de jour depuis la création du message
			r = age%(3600*24)#on récupère le reste de la division ci dessus
			h = int(r/3600)# on convertit le reste obtenue en heure
			r = r%3600 #on récupère le reste de la division ci dessus
			m = int(r/60)# on convertit le reste obtenue en minute
			s = r%60 # ce qu'il nous reste correspond au seconde
			age = (str(age)+"~"+str(d)+"d"+ str(h) + "h"+str(m)+"m"+str(s)+"s")# on affiche l'age sous le format "XXXd + XXXh + XXXm + XXXs"
		return age # on retourne l'age

	#testers of message type
	def is_i_said(self):
		return self.type == Message.MSG_ISAID# on teste si le type du message est MSG_ISAID  = 0
	def is_i_heard(self):
		return self.type == Message.MSG_IHEARD# on teste si le type du message est MSG_IHEARD = 1
	def is_they_said(self):
		return self.type == Message.MSG_THEY# on teste si le type du message est MSG_THEY = 2

	#setters
	def set_type(self, new_type):
		self.type = new_type# on donne au message le type voulu

	#a class method that generates a random message
	@classmethod
	def generate(cls):
		return sha256(bytes(str(time.time())+str(random()),'utf8')).hexdigest()

	#a method to convert the object to string data
	def __str__(self):
		return f"\t\t<message type=\"{self.type}\">\n\t\t\t<content>{self.content}</content>\n\t\t\t<date>{self.hdate()}</date>\n\t\t</message>"

	#export/import
	def m_export(self):
		return f"{{\"content\":\"{self.content}\",\"type\":{self.type}, \"date\":{self.date}}}"

	def m_import(self, msg):
		if(type(msg) == type(str())):#si le type de mon message est string
			json_object = json.loads(msg)
		elif(type(msg) == type(dict())):#si le type de mon message est un dictionnaire
			json_object = msg
		else:
			raise ValueError
		self.content = json_object["content"]
		self.type = json_object["type"]
		self.date = json_object["date"]

#will only execute if this file is run
if __name__ == "__main__":
	#test the class
	myMessage = Message() # on crée un message sans préciser son type
	time.sleep(1)#on met un délai de 1s avant de réaliser la ligne suivante
	mySecondMessage = Message(Message.generate(), Message.MSG_THEY)# on crée un message en précisant son type
	copyOfM = Message(myMessage.m_export())# on crée une copie du premier message en utilisant la fonction m_export
	print(myMessage)
	print(mySecondMessage)
	print(copyOfM)
	time.sleep(0.5)#on met un délai de 0,5 avant de réaliser la ligne suivante
	print(copyOfM.age(True,True))
	time.sleep(0.5)#on met un délai de 0,5 avant de réaliser la ligne suivante
	print(copyOfM.age(False,True))
