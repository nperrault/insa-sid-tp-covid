#!/usr/bin/env python3#!/usr/bin/env python3
from flask import Flask #server
from flask import request #to handle the different http requests
from flask import Response #to reply (we could use jsonify as well but we handled it)
from flask import render_template #to use external files for responses (out of the scope of the project, added to provide a test UI)
import json
#My libraries
from message import Message
from client import Client #embarks message_catalog

app = Flask(__name__)

#“Routes” will handle all requests to a specific resource indicated in the
#@app.route() decorator

#These route are for the "person" use case
	#case 1 refuse GET
@app.route('/', methods=['GET']) #on construit une première route pour obtenir la réponse du serveur. La route n'a pas besoin d'attributs de base.GET signifie qu'on récupère des ressources
def index(): # on crée la fonction index, qui n'a pas besoin d'attributs. 
	response = render_template("menu.html",root=request.url_root, h="") # on utilise une fonction flask générer une sortie de notre dossier template afin d'initialiser la réponse et importer le menu.html
	return response 

	#case 2 hear message
@app.route('/<msg>', methods=['POST']) #Lorsque l'on va rencontrer <msg>, on va appeler la fonction ci dessous. POST signifie qu'on crée des ressources
def add_heard(msg):# on crée la fonction index qui prend msg comme attribut
	if client.catalog.add_message(Message(msg, Message.MSG_IHEARD)):# on vérifie que le catalogue de notre client respecte la condition souhaitée
		response = Response(f"Message “{msg}” received.", mimetype='text/plain', status=201)# La condition est respectée, on met le statut = 201  afin de signaler que la requête est traitée avec succes et crée un élément réponse
	else :
		reponse = Response(status=400)# la condition n'est pas respectée, on met le statut = 400  afin de signaler que la requête est erronée.
	return response
#End of person use case

#Hospital use case
@app.route('/they-said', methods=['GET','POST']) #Lorsque l'on va rencontrer 'they said', on va appeler la fonction ci dessous. GET et POST signifie qu'on récupère des ressources et qu'on en crée des nouvelles
def hospital():# on crée la fonction hopital, qui n'a pas besoin d'attributs. 
	if request.method == 'GET': #on réalise la recupération de la  ressource
		#Wants the list of covid messages (they said)
		client.catalog.purge(Message.MSG_THEY)# on appelle la fonction purge du catalogue de notre client
		response = Response(client.catalog.c_export_type(Message.MSG_THEY), mimetype='application/json', status=200)#On crée l'élément réponse et on met statut a 200 pour indiquer que la requète a été faite avec succes
	elif request.method == 'POST':#on crée une ressource
		if request.is_json:
			req = json.loads(request.get_json())
			response = client.catalog.c_import_as_type(req, Message.MSG_THEY)
			response = Response(f"{response} new “they said” messages.", mimetype='text/plain', status=201)# on crée une réponse en mettant le statut = 200 pour indiquer que la requète a été réaliser correctement
		else:
			response = Response(f"JSON expected", mimetype='text/plain', status=400)#on crée une reponse en mettant le statut= 400 pour indiquer qu'il y a un problème de syntaxe dans la requète
	else:
		reponse = Response(f"forbidden method “{request.method}”.",status=403)#on crée une reponse en mettant le statut= 403 pour indiquer que l'acces à la ressource est interdit
	return response
#End hospital use case

#Begin UI
#These routes are out of the scope of the project, they are here
#to provide a test interface
@app.route('/check/<host>', methods=['GET'])
def check(host):
	h = host.strip()
	n = client.get_covid(h)
	r = dict()
	r["text"] = f"{n} they said messages imported from {h} ({client.r.status_code})"
	if client.catalog.quarantine(4):
		r["summary"] = "Stay home, you are sick."
	else:
		r["summary"] = "Everything is fine, but stay home anyway."
	return render_template("menu.html",responses=[r],root=request.url_root, h=h)

@app.route('/declare/<host>', methods=['GET'])
def declare(host):
	h = host.strip()
	client.send_history(h)
	r=[{"summary":f"Declare covid to {h} ({client.r.status_code})",
	"text":client.r.text}]
	return render_template("menu.html",responses=r,root=request.url_root, h=h)

@app.route('/say/<hosts>', methods=['GET'])
def tell(hosts):
	hosts = hosts.split(",")
	r=[]
	for host in hosts:
		h = host.strip()
		client.say_something(h)
		r.append({"summary":f"ISAID to {h} ({client.r.status_code})",
		"text":client.r.text})
	return render_template("menu.html", responses=r, root=request.url_root, h=h)
#end UI

#will only execute if this file is run
if __name__ == "__main__":
	debugging = True
	client = Client("client.json", debugging)
	app.run(host="0.0.0.0", debug=debugging)

