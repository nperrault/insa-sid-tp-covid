#!/usr/bin/env python3
from message import Message
from message_catalog import MessageCatalog
import requests

class Client:
	#constructor, takes a message catalog
	def __init__(self, catalog_path, debug = False, defaultProtocol="http://"):
		self.catalog = MessageCatalog(catalog_path) #initialise le catalogue
		if debug: #si le debug= True, on affiche le catalogue. 
			print(self.catalog)
		self.r = None # on initialise le reste des attributs
		self.debug = debug
		self.protocol = defaultProtocol

	@classmethod	
	def withProtocol(cls, host):
		res = host.find("://") > 1	#si on trouve le String (://) dans l'ensemble host, on associe res à la valeur correspondante
		return res # on retourne res

	def completeUrl(self, host, route = ""):
		if not Client.withProtocol(host): # Si la fonction withProtocol de  notre host  ne renvoie rien
			host = self.protocol + host #on met à jour la variable host
		if route != "": #si jamais notre route a une valeur
			route = "/"+route #on ajoute le "/" au début de la route de notre URL
		return host+route	#on renvoie la fusion de host et route pour avoir l'URL complète

	#send an I said message to a host
	def say_something(self, host):
		m = Message() # on crée un objet message
		self.catalog.add_message(m) #on ajoute le message au catalogue
		route = self.completeUrl(host, m.content) # on crée la route en utilisant la  fonction completeUrl
		self.r = requests.post(route) # on envoie la data a la ressource
		if self.debug:# si l'attribut debug est True,
			print("POST  "+route + "→" + str(self.r.status_code))# on affiche POST, la route de la commande et si celle ci est un succes ou pas
			print(self.r.text)
		return self.r.status_code == 201 #  la requète est un succes, le statut de celle ci est donc égal à 201

	#add to catalog all the covid from host server
	def get_covid(self, host):
		route = self.completeUrl(host,'/they-said')	# on crée la route en utilisant la  fonction completeUrl
		self.r = requests.get(route)	# on récupère la data a la ressource
		res = self.r.status_code == 200 #le statut de la requère est egal a 200, afin de montrer que la requète est OK, qu'elle fonctionne
		if res:	# si notre res est opérationnel
			res = self.catalog.c_import(self.r.json()) # On importe le data du catalogue
		if self.debug:	# si l'attribut debug est True
			print("GET  "+ route + "→" + str(self.r.status_code))# on affiche GET, la route de la commande et si celle ci est un succes ou pas
			if res != False: # si notre res est différent de non opérationnel
				print(str(self.r.json()))# on affiche le data du catalogue sous forme de string
		return res

	#send to server list of I said messages
	def send_history(self, host):
		route = self.completeUrl(host,'/they-said') # on crée la route en utilisant la  fonction completeUrl
		self.catalog.purge(Message.MSG_ISAID)	 # on supprime les messages qui ne nous interressent pas du catalogue
		data = self.catalog.c_export_type(Message.MSG_ISAID) #On récupère le ou les messages souhaités
		self.r = requests.post(route, json=data)
		if self.debug:	# si notre attribut debug est True
			print("POST  "+ route + "→" + str(self.r.status_code))# on affiche POST, la route de la commande et si celle ci est un succes ou pas
			print(str(data))
			print(str(self.r.text))
		return self.r.status_code == 201# On retourne le statut de la requète égale a 201 qui signifie que celle ci est bonne


if __name__ == "__main__":
	c = Client("client.json", True)# on crée un client
	c.say_something("localhost:5000")# on envoie un message au host
	c.get_covid("localhost:5000")#on ajoute tous les cas covid au catalogue du localhost 5000
	c.send_history("localhost:5000")# on envoie une liste de message au serveur localhost 5000
